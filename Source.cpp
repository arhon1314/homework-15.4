#include <iostream>
using namespace std;

int func()
{
	std::cout << "Enter any number: ";
	int n;
	cin >> n; 
	for (int x = 1; x <= n; x++)
	{
		if (n % 2 == 0 && x % 2 == 0)
		{
			std::cout << x << " ";
		}
		else if (n % 2 > 0 && x % 2 > 0)
		{
			std::cout << x << " ";
		}
	}
	return 0;
}

int main()
{
	int a = 21;
	for (int j = 1; j < a; ++j)
	{
		if (j % 2 == 0)
		{
			std::cout << j << " ";
		}
	}
	std::cout << "\n";
	func();
	return 0;
}